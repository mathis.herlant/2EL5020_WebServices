package rest_example;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

public class ChuckNorrisClient {
	public static void main(String[] args) {
		// Build the client, with the default settings (GET method, HTTP/2, default proxy...):
		HttpClient client = // YOUR CODE HERE
		
		// URI for the joke with specific id
		URI uri = // YOUR CODE HERE

		// request
		HttpRequest request = // YOUR CODE HERE
		try {
			// Receive the response body as a string:
			HttpResponse<String> response = // YOUR CODE HERE
			// Display response body
			System.out.println( // YOUR CODE HERE
		} catch (IOException ioe) {
			System.err.println("IO problem when communicating with the server: " + ioe);
		} catch (InterruptedException ie) {
			System.err.println("Interrupted while communicating with the server: " + ie);
		}
	}
}
