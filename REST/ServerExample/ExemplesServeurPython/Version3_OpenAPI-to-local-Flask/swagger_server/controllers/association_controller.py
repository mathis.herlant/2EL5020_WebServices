import connexion
import six

from swagger_server.models.member import Member  # noqa: E501
from swagger_server import util

# list of members
#----------------
# key = member id, 
# value = another dictionary with the keys name, birthYear, gender, plan
members = {}

lastId = 0


def add_member(body):  # noqa: E501
    """Add a new member

     # noqa: E501

    :param body: Information to create the new member
    :type body: dict | bytes

    :rtype: Member
    """
    global lastId
    global members
    json_member = connexion.request.get_json()
    name = json_member["name"]
    birthYear = json_member["birthYear"]
    gender = json_member["gender"]
    plan = json_member["plan"]
    member = {"name": name, "birthYear": birthYear, "gender": gender, "plan": plan}
    members[str(lastId)] = member
    lastId = lastId + 1
    return '''"{id}":{m}'''.format(id=lastId-1, m=member)



def delete_member(member_id):  # noqa: E501
    """Delete a member

     # noqa: E501

    :param member_id: ID of member to delete
    :type member_id: int

    :rtype: None
    """
    global members
    if str(member_id) not in members:
        # Returns HHTP status 404 Not Found
        return "Member not found", 404
    members.pop(str(member_id))
    return "Member {id} deleted".format(id = str(member_id))



def get_all_members():  # noqa: E501
    """List all members

    Returns a list of members # noqa: E501


    :rtype: List[Member]
    """
    return members


def get_member_by_id(member_id):  # noqa: E501
    """Find member by ID

    Returns a single member # noqa: E501

    :param member_id: ID of member to return
    :type member_id: int

    :rtype: Member
    """
    if str(member_id) not in members:
        # Returns HHTP status 404 Not Found
        return "Member not found", 404

    return '''"{id}":{m}'''.format(id=str(member_id), m=members[str(member_id)])



def reset():  # noqa: E501
    """Restart a fresh empty list of members

     # noqa: E501


    :rtype: str
    """
    global members
    members = {}
    global lastId
    lastId = 0
    return "Reset performed"



def update_member(body):  # noqa: E501
    """Update an existing member

     # noqa: E501

    :param body: Information to update an existent member
    :type body: dict | bytes

    :rtype: Member
    """
    global members
    json_member = connexion.request.get_json()
    id = json_member["id"]
    name = json_member["name"]
    birthYear = json_member["birthYear"]
    gender = json_member["gender"]
    plan = json_member["plan"]
    member = {"name": name, "birthYear": birthYear, "gender": gender, "plan": plan}
    members[str(id)] = member
    return '''"{id}":{m}'''.format(id=id, m=member)
