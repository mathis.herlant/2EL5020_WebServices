# Version 3: from OpenAPI to local Flask

Go to [https://editor.swagger.io/](https://editor.swagger.io/)

Clear file and edit using [membership_openapi.yml](https://gitlab-student.centralesupelec.fr/galtier/BioTech/-/blob/main/ExemplesCoteServeur/Version3_OpenAPI-to-local-Flask/membership_openapi.yaml).

Generate Server: select `python-flask`, download the generated zip.

Uncompress: `unzip python-flask-server-generated.zip`.

Open with VSCode. 0pen a Terminal and type
```
python3 -m venv .env
source .env/bin/activate
```

The `README.md` file of the generated server indicates:

> To run the server, please execute the following from the root directory:
> ```
> pip3 install -r requirements.txt
> python3 -m swagger_server
> ```
> and open your browser to here:
> ```
> http://localhost:8080/assocation/ui/
> ```


Edit the skeleton (swagger_server/controllers/association_controller.py) using this model: [swagger_server/controllers/association_controller.py](https://gitlab-student.centralesupelec.fr/galtier/BioTech/-/blob/main/ExemplesCoteServeur/Version3_OpenAPI-to-local-Flask/swagger_server/controllers/association_controller.py).

Test from Swagger UI.

