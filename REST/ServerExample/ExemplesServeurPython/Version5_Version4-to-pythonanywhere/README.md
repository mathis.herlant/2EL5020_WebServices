# Version 5: from Version 4 to PythonAnywhere

Go to [https://eu.pythonanywhere.com](https://eu.pythonanywhere.com)

## Create the MySQL DB

- Click Databases
- Make sure "MySQL" is selected
- Database name: `association`
- Create (your DB name is `username$association`)
- pick up a MySQL password: `<wxcvbn,`

Note: to delete a DB: click on it to open the mysql console and enter the command 
```
drop database username$association;
```

## Create the Flask application

- Open Web tab
- Add a new web app, next
- select Flask, Python 3.10
- path: /home/username/v5/membership.py, next
- go to v5 directory, edit the `membership.py` file
- copy [the `membership.py` file from Version 4](https://gitlab-student.centralesupelec.fr/galtier/BioTech/-/blob/main/ExemplesCoteServeur/Version4_local-Flask-with-DB/membership.py)
- remove the last line: `app.run()`
- adapt the connection line:
```
conn = mysql.connector.connect(host='username.mysql.eu.pythonanywhere-services.com', database="username$association", user="username", password="<wxcvbn,")
```
- Save and Run the file
- Test:
```
curl -X DELETE http://username.eu.pythonanywhere.com/reset

curl http://username.eu.pythonanywhere.com/members

curl -X POST http://username.eu.pythonanywhere.com/newMember \
    -H 'Content-Type: application/json' \
    -d '{
  "name": "Alice",
  "birthYear": 2001,
  "gender": "F",
  "plan": "regular"
}'

curl -X POST http://username.eu.pythonanywhere.com/newMember \
    -H 'Content-Type: application/json' \
    -d '{
  "name": "Bob",
  "birthYear": 2017,
  "gender": "M",
  "plan": "free"
}'

curl 'http://username.eu.pythonanywhere.com/ageRange?ageMin=10&ageMax=40'

curl http://username.eu.pythonanywhere.com/member/1

curl -X DELETE http://username.eu.pythonanywhere.com/member/1

curl http://username.eu.pythonanywhere.com/members
```