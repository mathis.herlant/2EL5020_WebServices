from flask import Flask
from flask import request

# creates a Flask application object, run at the last line of this code
#----------------------------------------------------------------------
# (__name__ is a built-in variable in Python,
# when the file is imported, __name__ contains the name of that module
# when the file is being run by itself, __name__ is '__main__')
app = Flask(__name__)

# list of members
#----------------
# key = member id, 
# value = another dictionary with the keys name, birthYear, gender, plan
members = {}

lastId = 0

# create routes
#--------------

# app is a Flask object, route() is a Flask method to be used in a decorator ("@")

# sets up a routing rule for incoming requests
# here the endpoint name for the route defaults to the "/" parameter 
# the "methods" parameter defaults to ['GET']

# There does not need to be any relationship between the decorator and the function 
# except proximity.
@app.route('/')
def welcome():
    # The return value is a string.
    # Flask converts it into a response object with the string as response body, 
    # a 200 OK status code and a text/html mimetype.
    return "Welcome on the membership tool!"

# This method changes the state of the resources -> should not use the HTTP GET method
@app.route('/reset', methods=['DELETE'])
def reset():
    global members
    members = {}
    global lastId
    lastId = 0
    return "Reset performed"

# Often people writing a Flask app use the same word for the route and the function,
# but this is not mandatory.
@app.route('/members', methods=['GET'])
def get_all_members():
    return members    

# Demonstrates the use of a variable in a Flask route (< >).
# Demonstrates how to get information on the incoming request (request.method).
# Default HTTP response status is 200. Demonstrates how to send back something else.
@app.route('/member/<member_id>', methods=['GET','DELETE'])
def member(member_id):
    global members
    if member_id not in members:
        # Returns HHTP status 404 Not Found
        return "Member not found", 404

    if request.method == 'GET':
        return '''"{id}":{m}'''.format(id=member_id, m=members[member_id])
        
    if request.method == 'DELETE':
        members.pop(member_id)
        return "Member {id} deleted".format(id = member_id)

# Demonstrates the POST method
@app.route('/newMember', methods=['POST'])
def add_member():
    global lastId
    global members
    json_member = request.get_json()
    name = json_member["name"]
    birthYear = json_member["birthYear"]
    gender = json_member["gender"]
    plan = json_member["plan"]
    member = {"name": name, "birthYear": birthYear, "gender": gender, "plan": plan}
    members[str(lastId)] = member
    lastId = lastId + 1
    return '''"{id}":{m}'''.format(id=lastId-1, m=member)

# Demonstrates the usage of url arguments: /ageRange?ageMin=10&ageMax=40
@app.route('/ageRange', methods=['GET'])
def get_members_in_age_range():
    ageMin = int(request.args.get("ageMin"))
    ageMax = int(request.args.get("ageMax"))
    members_in_range = {}
    for memberId, member in members.items():
        age = 2023 - int(member["birthYear"])
        if age >= ageMin and age <= ageMax:
            members_in_range[memberId] = member
    return members_in_range 

app.run()