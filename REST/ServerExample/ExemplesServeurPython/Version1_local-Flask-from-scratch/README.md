# Version 1: local Flask from scratch

We need to build a web server application (a program always on, that can receive HTTP requests and reply with HTTP responses). Although it’s possible to use Python to build it from scratch, it's simpler to use [Flask](https://flask.palletsprojects.com), a popular web application framework, which provides a collection of modules that help you build server-side web applications. It’s technically a micro web framework, in that it provides the minimum set of technologies needed for this task. This means Flask is not as feature-full as some of its competitors—such as Django, the mother of all Python web frameworks—but it is small, lightweight, and easy to use.
As our requirements aren’t heavy, Flask is more than enough for us at this time.

## Getting started with Flask

Create a new virtual environment and install Flask:

```
python3 -m venv .env
source .env/bin/activate
pip install flask
```

Write the minimal version of the `membership.py` file:
```
from flask import Flask
from flask import request

app = Flask(__name__)

@app.route('/')
def welcome():
    return "Welcome on the membership tool!"

app.run()
```
Run the program.

Flask confirms it is up and running and waiting to service web requests at Flask’s test web address (127.0.0.1) and protocol port number (5000):
* Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
If you see this message, all is well.
Flask’s web server is ready and waiting.

Open your browser and type http://127.0.0.1:5000/

The “Welcome on the membership tool!” message from `membership.py` should appear in your browser’s window.
In addition to this, take a look at the terminal window where your webapp is running. A new status message should’ve appeared too.

## Add routes for our app

Look at [membership.py](https://gitlab-student.centralesupelec.fr/galtier/BioTech/-/blob/main/ExemplesCoteServeur/Version1_local-Flask-from-scratch/membership.py). 

The example demonstrates:
- how to build routes for different methods: GET, DELETE, POST
- how to retrieve url variables: (such as `/members/3`)
- how to retrieve parameters provided in query (using `request.args`)
- how to retrieve data provided in JSON body (using `request.get_json`)
- how to return HTTP error status `404` 

You can test with [RESTer pluggin](https://chrome.google.com/webstore/detail/rester/eejfoncpjfgmeleakejdcanedmefagga?hl=en=), or use [curl](https://curl.se/):

```
curl -X DELETE http://127.0.0.1:5000/reset

curl http://127.0.0.1:5000/members

curl -X POST http://127.0.0.1:5000/newMember \
    -H 'Content-Type: application/json' \
    -d '{
  "name": "Alice",
  "birthYear": 2001,
  "gender": "F",
  "plan": "regular"
}'

curl -X POST http://127.0.0.1:5000/newMember \
    -H 'Content-Type: application/json' \
    -d '{
  "name": "Bob",
  "birthYear": 2017,
  "gender": "M",
  "plan": "free"
}'

curl 'http://127.0.0.1:5000/ageRange?ageMin=10&ageMax=40'

curl http://127.0.0.1:5000/member/1

curl -X DELETE http://127.0.0.1:5000/member/1

curl http://127.0.0.1:5000/members
```