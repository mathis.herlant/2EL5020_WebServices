# Version 4: from Version 1, with a DB

## Start MySQL server and create the DB
SQLite is fine for PoC but to use with Flask and pythonanywhere it's easier to use mysql. Use a docker image with mysql instead of installing it:
```
docker run --name mysql-container -d -p 3306:3306 -v /mypath/ExemplesCoteServeur/Version4_local-Flask-with-DB/mysql-container-volume:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=azerty mysql:8
```
(`mypath`... refers to the host repository where to save the DB (otherwise the DB will be lost when the container is stopped))

Connect to the container, and run the SQL command to create the DB and the table:
```
docker exec -it mysql-container mysql -p
```
```
CREATE DATABASE association;
```

Optional, to review SQL commands:
- define the base as working base: 
```
USE association;
```
- create the table: 
```
CREATE TABLE IF NOT EXISTS Members(id INTEGER PRIMARY KEY AUTO_INCREMENT, name TEXT, birthYear INTEGER, gender TEXT, plan TEXT);
```
- add 2 rows:
```
	INSERT INTO Members(name, birthYear, gender, plan) VALUES ("Alice", 2001, "F", "regular");

	INSERT INTO Members(name, birthYear, gender, plan) VALUES ("Bob", 2019, "M", "free");
```
- and read the table: 
```
SELECT * FROM Members;
```

## Write the Python program to read and write to the DB

Create a new virtual environment and install the MySQL driver:
```
python3 -m venv .env
source .env/bin/activate
pip install mysql-connector-python
```

Modify the [Version 1](https://gitlab-student.centralesupelec.fr/galtier/BioTech/-/blob/main/ExemplesCoteServeur/Version1_local-Flask-from-scratch/membership.py) of the Python program to read and write from the DB instead of using a dictionary to store data. See [the Version 3 of the `membership.py` file](https://gitlab-student.centralesupelec.fr/galtier/BioTech/-/blob/main/ExemplesCoteServeur/Version4_local-Flask-with-DB/membership.py)

Test:
```
curl -X DELETE http://127.0.0.1:5000/reset

curl http://127.0.0.1:5000/members

curl -X POST http://127.0.0.1:5000/newMember \
    -H 'Content-Type: application/json' \
    -d '{
  "name": "Alice",
  "birthYear": 2001,
  "gender": "F",
  "plan": "regular"
}'

curl -X POST http://127.0.0.1:5000/newMember \
    -H 'Content-Type: application/json' \
    -d '{
  "name": "Bob",
  "birthYear": 2017,
  "gender": "M",
  "plan": "free"
}'

curl http://127.0.0.1:5000/members

curl http://127.0.0.1:5000/member/1

curl http://127.0.0.1:5000/members

curl -X DELETE http://127.0.0.1:5000/member/1

curl http://127.0.0.1:5000/members

curl -X POST http://127.0.0.1:5000/newMember \
    -H 'Content-Type: application/json' \
    -d '{
  "name": "Charly",
  "birthYear": 2007,
  "gender": "M",
  "plan": "free"
}'

curl 'http://127.0.0.1:5000/ageRange?ageMin=3&ageMax=7'
```
