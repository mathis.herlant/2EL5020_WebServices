from flask import Flask
from flask import request

import mysql.connector

# creates a Flask application object, run at the last line of this code
#----------------------------------------------------------------------
app = Flask(__name__)

# DB connexion objects
#---------------------
conn = mysql.connector.connect(host='127.0.0.1', database="association", user="root", password="azerty")
cursor = conn.cursor(prepared=True)

def create_table():
#    conn.start_transaction() # automatic with mysql
    try:
        cursor.execute('''
            CREATE TABLE IF NOT EXISTS Members(
                id INTEGER PRIMARY KEY AUTO_INCREMENT,
                name TEXT,
                birthYear INT,
                gender TEXT,
                plan TEXT
            );
        ''')
        conn.commit()
        return True
    except mysql.connector.Error as error:
        print("Problem creating the table: ", format(error))
        conn.rollback()
        return False

def drop_table():
#    conn.start_transaction()
    try:
        cursor.execute("DROP TABLE IF EXISTS Members;")
        conn.commit()
        return True
    except mysql.connector.Error as error:
        print("Problem dropping the table: ", format(error))
        conn.rollback()
        return False


# create routes
#--------------

@app.route('/')
def welcome():
    return "Welcome on the membership tool!"

@app.route('/reset', methods=['DELETE'])
def reset():
    if drop_table() and create_table():
        return "Reset performed"
    else:
        return "Reset was not performed properly", 500

@app.route('/members', methods=['GET'])
def get_all_members():
    try:
        query = "SELECT * FROM Members;"
        cursor.execute(query)
        data = cursor.fetchall()
        print(data)
        members = {}
        for id, name, birthYear, gender, plan in data:
            members[id] = {"name": name, "birthYear": birthYear, "gender": gender, "plan": plan}
        return members
    except mysql.connector.Error as error:
        return("Problem retrieving the members: ", format(error)), 500       

@app.route('/member/<member_id>', methods=['GET','DELETE'])
def member(member_id):
    if request.method == 'GET':
        try:
            query = "SELECT * FROM Members WHERE id = ?;"
            query_values = (member_id, )
            cursor.execute(query, query_values)
            member = cursor.fetchone()
            if member is not None:
                (id, name, birthYear, gender, plan) = member
                return {id: {"name": name, "birthYear": birthYear, "gender": gender, "plan": plan}}
            else:
                return "Member not found", 404
        except mysql.connector.Error as error:
            return("Problem retrieving the member: ", format(error)), 500
        
    if request.method == 'DELETE':
#        conn.start_transaction()
        try:
            query = "DELETE FROM Members WHERE id = ?;"
            query_values = (member_id, )
            cursor.execute(query, query_values)
            conn.commit()
            return "Member " + str(member_id) + " deleted"
        except mysql.connector.Error as error:
            conn.rollback()
            return("Problem deleting the member: ", format(error)), 500


@app.route('/newMember', methods=['POST'])
def add_member():
    json_member = request.get_json()
    name = json_member["name"]
    birthYear = json_member["birthYear"]
    gender = json_member["gender"]
    plan = json_member["plan"]
#    conn.start_transaction()
    try:
        insert_query = "INSERT INTO Members (name, birthYear, gender, plan) VALUES (?, ?, ?, ?)"
        query_values = (name, birthYear, gender, plan)
        cursor.execute(insert_query, query_values)
        conn.commit()
        id = cursor.lastrowid
        member = {}
        member[id] = {"name": name, "birthYear": birthYear, "gender": gender, "plan": plan}
        return member
    except mysql.connector.Error as error:
        print("Problem inserting the new member: ", format(error))
        conn.rollback()
        return False


@app.route('/ageRange', methods=['GET'])
def get_members_in_age_range():
    ageMin = int(request.args.get("ageMin"))
    ageMax = int(request.args.get("ageMax"))
    yearMin = 2023 - ageMax
    yearMax = 2023 - ageMin
    try:
        query = "SELECT * FROM Members WHERE birthYear >= ? AND birthYear <= ?;"
        cursor.execute(query, (yearMin, yearMax))
        data = cursor.fetchall()
        members = {}
        for id, name, birthYear, gender, plan in data:
            members[id] = {"name": name, "birthYear": birthYear, "gender": gender, "plan": plan}
        return members
    except mysql.connector.Error as error:
        return("Problem retrieving the members: ", format(error)), 500  

app.run()