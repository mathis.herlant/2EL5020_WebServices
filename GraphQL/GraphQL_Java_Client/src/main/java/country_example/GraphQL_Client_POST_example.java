package country_example;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

public class GraphQL_Client_POST_example {
	public static void main(String[] args) {
		// Build the client with the default settings (GET method, HHTP/2, default proxy...):
		HttpClient client = HttpClient.newHttpClient();

		// URI
		String endPoint = "https://countries.trevorblades.com";
		String query = "{ \n" + "\"operationName\": null, \n"
				+ "\"query\": \"{\\n  country(code: \\\"FR\\\") {\\n    name\\n    continent {\\n      name\\n    }\\n    capital\\n    currency\\n  }\\n}\", \n"
				+ "\"variables\": {} \n" + "}";

		URI uri = URI.create(endPoint);

		// request
		HttpRequest request = HttpRequest.newBuilder().uri(uri).header("Content-Type", "application/json")
				.POST(BodyPublishers.ofString(query)).build();
		try {
			// Receive the response body as a string:
			HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
			// Display response body
			System.out.println(response.body());
		} catch (IOException ioe) {
			System.err.println("IO problem when communicating with the server: " + ioe);
		} catch (InterruptedException ie) {
			System.err.println("Interrupted while communicating with the server: " + ie);
		}
	}
}
