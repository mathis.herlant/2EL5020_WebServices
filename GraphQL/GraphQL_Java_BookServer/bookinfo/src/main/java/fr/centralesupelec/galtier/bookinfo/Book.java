package fr.centralesupelec.galtier.bookinfo;

public class Book {

	private String id;
	private String title;
	private int pageCount;
	private Author author;

	public Book(String id, String title, int pageCount, Author author) {
		this.id = id;
		this.title = title;
		this.pageCount = pageCount;
		this.author = author;
	}

	public String getId() {
		return id;
	}

	public String getAuthorId() {
		return author.getId();
	}
}
