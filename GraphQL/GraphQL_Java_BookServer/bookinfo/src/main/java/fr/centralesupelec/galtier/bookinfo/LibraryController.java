package fr.centralesupelec.galtier.bookinfo;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.stereotype.Controller;

@Controller
public class LibraryController {

	@QueryMapping
	public Book bookById(@Argument String id) {
		System.out.println("bookById(" + id + ")");
		return Library.getBookById(id);
	}

	@QueryMapping
	public Author authorByLastName(@Argument String lastName) {
		System.out.println("authorByName(" + lastName + ")");
		return Library.getAuthorByLastName(lastName);
	}

	@SchemaMapping
	public Author author(Book book) {
		return Library.getAuthorById(book.getAuthorId());
	}
}