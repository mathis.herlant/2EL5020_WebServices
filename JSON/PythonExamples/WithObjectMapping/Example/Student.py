class Student(object):
    def __init__(self, name, age):
        self.name = name
        self.age = age
        self.clubs = []
        self.grades = {}

    def addClub(self, club):
        self.clubs.append(club)

    def addGrade(self, subject, grade):
        self.grades[subject] = grade

    def getName(self):
        return self.name

    def getAge(self):
        return self.age

    def getClubs(self):
        return self.clubs

    def getGrades(self):
        return self.grades

    def __str__(self):
        description = f"{self.name} is {self.age}"
        if len(self.clubs) == 0:
            description += " and doesn't belong to any club."
        else:
            description += " and belongs to the following club(s): "
            for club in self.clubs:
                description += f"{club} "
        return description