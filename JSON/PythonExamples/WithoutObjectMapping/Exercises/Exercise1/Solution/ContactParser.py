import json

def getEmail(jsonFileName, name):
    jsonFile = open(jsonFileName)
    contacts = json.load(jsonFile)
    jsonFile.close()
    persons = contacts["people"]
    for person in persons:
        if person["name"] == name:
            return person["email"]
    return None

print(getEmail("addressBook.json", "John Doe"))
print(getEmail("addressBook.json", "Foo"))
