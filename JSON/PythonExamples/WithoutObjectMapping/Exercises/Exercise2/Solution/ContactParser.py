import json

def getnbPhoneNumbers(jsonFileName, name):
    jsonFile = open(jsonFileName)
    contacts = json.load(jsonFile)
    jsonFile.close()
    persons = contacts["people"]
    for person in persons:
        if person["name"] == name:
            return len(person["phoneNumbers"])
    return 0

print(getnbPhoneNumbers("addressBook.json", "John Doe"))
print(getnbPhoneNumbers("addressBook.json", "Tartempion Untel"))
print(getnbPhoneNumbers("addressBook.json", "Foo"))
