import json

# the JSON string to parse:
jsonString = """{
   "name":"Charly",
   "age":20,
   "clubs":[
      "reading",
      "cooking"
   ],
   "grades":{
      "maths":17,
      "computer science":16
   }
}"""

# create a dictionary from the JSON string:
charly = json.loads(jsonString)


# print the first-level keys-values
# along with the type of each value (primitive, array, or nested JSON object)
for k,v in charly.items():
    t = ''
    if type(v) == int or type(v) == str or type(v) == bool:
        t = 'primitive'
    if type(v) == list:
        t = 'array'
    if type(v) == dict:
        t = 'nested JSON object'
    print(k, ":", v, " type:", t)

# print a primitive value:
print("The name of Charly is:", charly["name"])
print("The age of Charly is:", charly["age"])

# iterate through a Json array:
print("The clubs Charly is a member of:")
clubs = charly["clubs"]
for c in clubs:
    print("\t-",c)

# get a value which is a nested JSON object:
grades = charly["grades"]
# and access to one of its primitive values:
print("Charly's grade in maths:", grades["maths"])