package exercise;

import java.util.List;

class AddressBook {
	private List<Contact> people;

	public AddressBook(List<Contact> contacts) {
		super();
		this.people = contacts;
	}

	public List<Contact> getContacts() {
		return people;
	}
}
