package example1;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

class Student {
	private String name;
	private int age;
	private List<String> clubs;
	private Map<String, Integer> grades;

	Student(String name, int age) {
		this.name = name;
		this.age = age;
		clubs = new Vector<String>();
		grades = new HashMap<String, Integer>();
	}

	void addClub(String club) {
		clubs.add(club);
	}

	void addGrade(String subject, int grade) {
		grades.put(subject, grade);
	}

	String getName() {
		return name;
	}

	int getAge() {
		return age;
	}

	List<String> getClubs() {
		return clubs;
	}

	Map<String, Integer> getGrades() {
		return grades;
	}

	public String toString() {
		String description = name + " is " + age;
		if (clubs.size() == 0)
			description += " and doesn't belong to any club.";
		else {
			description += " and belongs to the following club(s): ";
			for (String club : getClubs()) {
				description += club + " ";
			}
		}
		return description;
	}
}
