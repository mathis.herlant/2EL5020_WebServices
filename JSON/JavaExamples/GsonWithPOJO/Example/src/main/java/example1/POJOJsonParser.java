package example1;

import com.google.gson.Gson;

public class POJOJsonParser {

	public static void main(String[] arg) {
		// The JSON string to parse:
		String jsonString = "{\n"
				+ "   \"name\":\"Charly\",\n"
				+ "   \"age\":20,\n"
				+ "   \"clubs\":[\n"
				+ "      \"reading\",\n"
				+ "      \"cooking\"\n"
				+ "   ],\n"
				+ "   \"grades\":{\n"
				+ "      \"maths\":17,\n"
				+ "      \"computer science\":16\n"
				+ "   }\n"
				+ "}";
		// The Gson instance used to parse the string:
		Gson gsonParser = new Gson();

		// Create a Java object from the string:
		Student charly = gsonParser.fromJson(jsonString, Student.class);

		System.out.println(charly);
	}
}
