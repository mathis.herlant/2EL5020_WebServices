package example1;

import java.util.Map.Entry;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class SimpleJsonParser {

	public static void main(String[] arg) {
		// The JSON string to parse:
		String jsonString = "{\n"
				+ "   \"name\":\"Charly\",\n"
				+ "   \"age\":20,\n"
				+ "   \"clubs\":[\n"
				+ "      \"reading\",\n"
				+ "      \"cooking\"\n"
				+ "   ],\n"
				+ "   \"grades\":{\n"
				+ "      \"maths\":17,\n"
				+ "      \"computer science\":16\n"
				+ "   }\n"
				+ "}";
		// The Gson instance used to parse the string:
		Gson gsonParser = new Gson();

		// Create a JSON tree from the string:
		JsonObject charly = gsonParser.fromJson(jsonString, JsonObject.class);

		// Get a map of the first-level keys-values 
		// and print it along with the type of each value (primitive, array, or nested JSON object):
		System.out.println("elements of Charly:");
		for (Entry<String, JsonElement> element : charly.asMap().entrySet()) {
			System.out.println(element.getKey() + " : " + element.getValue() + " type " + element.getValue().getClass().getName());
		}
		// Get a primitive value 
		// (ie a value that is directly a string or number, not an array or a nested JSON object:
		System.out.println("The name of Charly is: " + charly.getAsJsonPrimitive("name"));
		System.out.println("The age of Charly is: " + charly.getAsJsonPrimitive("age"));
		// Get a JsonArray value and iterate through its content:
		System.out.println("The clubs Charly is a member of:");
		JsonArray clubsArray = charly.getAsJsonArray("clubs");
		for (JsonElement club : clubsArray) {
			System.out.println("\t- " + club);
		}
		// Get a value which is a nested JSON object:
		JsonObject grades = charly.getAsJsonObject("grades");
		// and access to one of its primitive values:
		System.out.println("Charly's grade in maths: " + grades.getAsJsonPrimitive("maths"));
	}
}
