import requests
URL = "http://columbia.edu/~fdc/sample.html"
# identity -> to get uncompressed content (see https://datatracker.ietf.org/doc/html/rfc9110#field.accept-encoding)
response = requests.get(url=URL, headers={"Range": "bytes=10-50", "Accept-Encoding": "identity;q=0"})
print(response.text)
