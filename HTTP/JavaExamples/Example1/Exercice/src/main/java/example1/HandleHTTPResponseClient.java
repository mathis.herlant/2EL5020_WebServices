package example1;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Version;
import java.net.http.HttpHeaders;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.net.ssl.SSLSession;

public class HandleHTTPResponseClient {

	public static void main(String[] args) {
		new HandleHTTPResponseClient();
	}
	
	/**
	 * Makes up 2 HTTP responses to test the displayHttpResponse method
	 */
	HandleHTTPResponseClient() {
		HttpResponse<String> response = httpResponseWithBody();
		displayHttpResponse(response);
		response = httpResponseWithoutBody();
		displayHttpResponse(response);		
	}
	
	/**
	 * Displays a HTTP response as specified in the exercise
	 * @param response the HTTP response to display
	 */
	public static void displayHttpResponse(HttpResponse<String> response) {
		// YOUR CODE HERE
	}

	/**
	 * Returns a made-up HTTP response similar to the reply receive to 
	 * a HTTP GET on http://www.brainjar.com/java/host/test.html  
	 */
	public static HttpResponse<String> httpResponseWithBody() {
		int statusCode = 200;
		URI uri = URI.create("http://www.brainjar.com/java/host/test.html");
		HttpRequest request = HttpRequest.newBuilder().uri(uri).build();
		Optional<HttpResponse<String>> previousResponse = Optional.empty();
		Map<String, List<String>> headerMap = Map.of("accept-ranges", List.of("bytes"), 
				"content-length", List.of("308"),
				"content-type", List.of("text/html"),
				"date", List.of("Mon, 20 Feb 2023 12:31:52 GMT"),
				"etag", List.of("\"0d85676324c31:0\""),
				"last-modified", List.of("Tue, 27 May 2003 15:17:04 GMT"), 
				"server", List.of("Microsoft-IIS/8.5"),
				"x-powered-by", List.of("ASP.NET"));
		HttpHeaders headers = HttpHeaders.of(headerMap, (s1,s2) -> true);
		Path filename = Path.of("page.html");
		String body = "<html>\n"
				+ "<head>\n"
				+ "<title>Test HTML File</title>\n"
				+ "</head>\n"
				+ "<body>\n"
				+ "\n"
				+ "<p>This is a very simple HTML file.</p>\n"
				+ "\n"
				+ "</body>\n"
				+ "</html>\n";
		try {
			body = Files.readString(filename);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Optional<SSLSession> sslSession = Optional.empty();
		HttpClient.Version version = Version.HTTP_1_1;
		return new FakeHttpResponse<String>(statusCode, request, previousResponse,
				headers, body, sslSession, uri, version);
	}

	/**
	 * Returns a made-up HTTP response similar to the reply receive to 
	 * a HTTP HEAD on http://www.brainjar.com/java/host/test.html  
	 */
	public static HttpResponse<String> httpResponseWithoutBody() {
		int statusCode = 200;
		URI uri = URI.create("http://www.brainjar.com/java/host/test.html");
		HttpRequest request = HttpRequest.newBuilder().method("HEAD", HttpRequest.BodyPublishers.noBody()).uri(uri).build();
		Optional<HttpResponse<String>> previousResponse = Optional.empty();
		Map<String, List<String>> headerMap = Map.of("accept-ranges", List.of("bytes"), 
				"content-length", List.of("308"),
				"content-type", List.of("text/html"),
				"date", List.of("Mon, 20 Feb 2023 12:31:52 GMT"),
				"etag", List.of("\"0d85676324c31:0\""),
				"last-modified", List.of("Tue, 27 May 2003 15:17:04 GMT"), 
				"server", List.of("Microsoft-IIS/8.5"),
				"x-powered-by", List.of("ASP.NET"));
		HttpHeaders headers = HttpHeaders.of(headerMap, (s1,s2) -> true);
		String body = "";
		Optional<SSLSession> sslSession = Optional.empty();
		HttpClient.Version version = Version.HTTP_1_1;
		return new FakeHttpResponse<String>(statusCode, request, previousResponse,
				headers, body, sslSession, uri, version);
	}

}
