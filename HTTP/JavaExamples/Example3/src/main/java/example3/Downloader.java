package example3;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse.BodyHandlers;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Downloader {
	public static void main(String[] args) {
		HttpClient client = HttpClient.newHttpClient();
		URI uri = URI.create("http://www.brainjar.com/java/host/test.html");
		Path path = Paths.get("/tmp/file.html");
		HttpRequest request = HttpRequest.newBuilder(uri).build();
		try {
			client.send(request, BodyHandlers.ofFile(path));
		} catch (IOException ioe) {
			System.err.println("IO problem when communicating with the server: " + ioe);
		} catch (InterruptedException ie) {
			System.err.println("Interrupted while communicating with the server: " + ie);
		}
	}
}
