package example5;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

public class POSTClient {
	public static void main(String[] args) {
		HttpClient client = HttpClient.newHttpClient();
		URI uri = URI.create("http://httpbin.org/post");
		HttpRequest request = HttpRequest.newBuilder()
				.uri(uri)
				.POST(BodyPublishers.ofString("hello"))
				.build();
		try {
			HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
			System.out.println(response.body());
		} catch (IOException ioe) {
			System.err.println("IO problem when communicating with the server: " + ioe);
		} catch (InterruptedException ie) {
			System.err.println("Interrupted while communicating with the server: " + ie);
		}
	}

}
