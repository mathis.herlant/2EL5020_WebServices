package example2;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Version;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

public class SimpleHTTPClient {
	public static void main(String[] args) {
		HttpClient client = // YOUR CODE HERE
		URI uri = URI.create("http://www.brainjar.com/java/host/test.html");
		HttpRequest request = // YOUR CODE HERE
		try {
			// Receive the response body as a string:
			HttpResponse<String> response = // YOUR CODE HERE
			System.out.println(response.body());
		} catch (IOException ioe) {
			System.err.println("IO problem when communicating with the server: " + ioe);
		} catch (InterruptedException ie) {
			System.err.println("Interrupted while communicating with the server: " + ie);
		}
	}
}
